/**
 * @license magic angularmods
 * (c) 2014-2015 ApolloC Software, Inc. http://www.apolloc.com
 * License: MIT
 */

var magicFactory;

(function(){
  var singleton;
  magicFactory = function(){
    return singleton || (singleton = new magicMessage());
  }
  
  //new linkedList().test();

  var magic = angular.module("magicModule", []);
  magic.directive("magicView", function($compile){
    return {
      scope: {
        view: '='
      }
      ,restrict: 'E'
      ,link: function ($scope, $element, $attr) {
        $scope.$watch('view', function (value) {
            cleanUpChild();
            if ($scope.view) {
              $element.html("<" + $scope.view.name + " view-info=\"view\"/><" + $scope.view.name + ">");
              $compile($element.contents())($scope);
            } 
        });

        function cleanUpChild(){
          $scope.$$childHead && $scope.$$childHead.$destroy && $scope.$$childHead.$destroy();
          $element.html("");
        }
      }
    }
  });

  magic.provider("magicMessage", function(){
    var me = this;
    me.$get = magicFactory;
  });

    function magicMessage(){
    var t = this;
    //root-->messageNameQueue-->subscription-->{receiver, callback}
    var root = {};
    t.subscribe = function(messageName, subscriber){
      var queue = root[messageName] || (root[messageName] = new linkedList());
      var exist = queue.has(subscriber);
      exist || queue.add(subscriber);
    }
    
    t.unsubscribe = function(messageName, subscriber){
      var queue = root[messageName] || (root[messageName] = new linkedList());
      var exist = queue.has(subscriber);
      exist && queue.remove(subscriber);
    }
    t.broadcast = function(messageName, message, sender){
      var queue = root[messageName] || (root[messageName] = new linkedList());
      queue.forEach(function(subscriber){
        subscriber[messageName] && subscriber[messageName].apply(subscriber, [message, sender]);
      });
    }

    t.safeApply = function ($scope, fn) {
      var phase = $scope.$root.$$phase;
      if(phase == '$apply' || phase == '$digest') {
          if (fn) {
              $scope.$eval(fn);
          }
      } 
      else {
        if (fn) {
            $scope.$apply(fn);
        } 
        else {
            $scope.$apply();
        }
      }
    }
  }


  //Need to do more testing on these link things...
  function linkItem(item, previousLinkItem){
    this.item = item;
    previousLinkItem && (previousLinkItem.nextLink = item);
  }

  function linkedList(){
    var t = this;
    t.head = null;
    t.tail = null;

    t.add = function(item){
      var lastLink = t.tail;
      var link = new linkItem(item);
      t.head || (t.head = link);
      t.tail || (t.tail = link);
      !(t.tail === link) && (t.tail.nextLink = link);
      t.tail = link; 
      return t;
    }

    t.remove = function(item){
      if (t.head && t.head.item === item){
        t.head = t.head.nextLink;
        if (t.tail && t.tail === item){
          t.tail = null;
        }
      }

      var link = t.head;
      var previousLink = null;
      while(link){
        if (link.item === item){
          previousLink && (previousLink.nextLink = link.nextLink);
        }
        previousLink = link;
        link = link.nextLink;
      }
      return t;
    }

    t.has = function (item){
      var exists = false;
      t.forEach(function(currItem){
        if (currItem === item){
          exists = true;
        }
      });
      return exists;
    }

    t.forEach = function(func){
      var link = t.head;
      while(link){
        func(link.item);
        link = link.nextLink;
      }
      return t;
    }

    t.test = function(){

      //Add multiple items
      var list = new linkedList();
      var item1 = {name:"item1"}, item2 = {name:"item2"}, item3 = {name:"item3"}, item4 = {name:"item4"};
      list.add(item1).add(item2).add(item3).add(item4);
      assert(list.has(item1));
      
      //Remove items
      list = new linkedList();
      list.add(item1);
      list.remove(item1);
      assert(!list.has(item1));

      //Test remove tail
      list = new linkedList();
      var item1 = {name:"item1"}, item2 = {name:"item2"}, item3 = {name:"item3"}, item4 = {name:"item4"};
      list.add(item1).add(item2).add(item3).add(item4);
      list.remove(item4);
      assert(!list.has(item4));
      assert(list.has(item2));
      assert(list.has(item3));
      assert(list.has(item1));
      
      //Test remove head
      list = new linkedList();
      var item1 = {name:"item1"}, item2 = {name:"item2"}, item3 = {name:"item3"}, item4 = {name:"item4"};
      list.add(item1).add(item2).add(item3).add(item4);
      list.remove(item1);
      assert(!list.has(item1));
      assert(list.has(item2));
      assert(list.has(item3));
      assert(list.has(item4));

      //Test remove middle
      list = new linkedList();
      var item1 = {name:"item1"}, item2 = {name:"item2"}, item3 = {name:"item3"}, item4 = {name:"item4"};
      list.add(item1).add(item2).add(item3).add(item4);
      list.remove(item3);
      assert(!list.has(item3));
      assert(list.has(item2));
      assert(list.has(item1));
      assert(list.has(item4));
    }

    function assert(condition, message){
      if (!condition){
        throw message || "Assert failed";
      }
    }
  }
})();
