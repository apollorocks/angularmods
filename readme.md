# angularmods #

Just some useful utilities for solving some things.  Much of this is gathered around the net.

Free code. Use at your own risk.

# magicView #
Magic View is an element directive that creates and displays other directives dynamically.  Magic View expects directives to define "view" on their attribute definitions.  View data is passed into the directive.  The view object is defined as {name:"", data:""}.  Up to the controller of the Magic View attribute to pass in the view object via Magic View's view attribute.  

# magicMessage #
Magic Message allows subscribers to register for messages by name.  Publishers broadcast messages by message name and subscribers handle them by defining a function with the same name as the message name.  A little slick :) 

What's cool is that Magic Message can be fired from within angular artifacts or within regular javascript.  The Magic Message safeApply function allows updating of angular elements without the dreaded apply phase error.

# Better Documentation and Examples to come #
Just whipped this up over the weekend :)