var app = angular.module("mainApp", ['magicModule']);
app.controller("mainController", ['$scope','magicMessage', function(scope, magicMessage){
  scope.currentView = null;
  scope.links = [{name:"Bookstore", view:{name:"view-bookstore", data:{}}}
  ,{name:"Customer", view:{name:"view-customer", data:{}}}
  ];

  scope.openView = function(link){
    if (!(scope.currentView === link.view)){
      scope.closeView(link.view);
    }

    scope.currentView = link.view;
  }

  scope.closeView = function(view){
    view && magicMessage.broadcast('onClosingView', view, scope);
  }

  scope.subscribe = function(){
      magicMessage.subscribe("onCloseView", scope);
  }

  scope.onCloseView = function(message, sender){
    (scope.currentView === message) && (scope.currentView = null);
  }

  scope.subscribe();
}]);