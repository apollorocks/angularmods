app.directive("viewCustomer", ['magicMessage', function(magicMessage){
  return{
    scope:  {
      //viewInfo is how viewMagic passes the data to this view
      viewInfo: '='
    }
    ,restrict: 'E'
    ,templateUrl: "appscripts/demo001/viewCustomer.html"
    ,link: function(scope, element){

      //Define scope properties the GUI binds to
      scope.customer = {};

      //Define scope functions that GUI invokes for user actions
      scope.onClosingView = function(message, sender){
        magicMessage.safeApply(scope, function(){
          //Makes sure the view data gets saved for next time this view is displayed
          scope.viewInfo.data = scope.customer;
        });
      }

      scope.$on('$destroy', function(){
        scope.unsubscribe();
      });

      scope.subscribe = function(){
        magicMessage.subscribe("onClosingView", scope);
      }

      scope.unsubscribe = function(){
        magicMessage.unsubscribe('onClosingView', scope);
      }

      scope.clickClose = function(){
        magicMessage.broadcast("onCloseView", scope.viewInfo, scope);
      }
      //Initialize here
      scope.subscribe();

      scope.$watch('viewInfo', function(){
        //This really depends on how the viewInfo was initialized.  Could be an id that is used to retrieve from the db
        if (scope.viewInfo && scope.viewInfo.data){
          scope.customer = scope.viewInfo.data;
        }
      });
    }
  }
}]);